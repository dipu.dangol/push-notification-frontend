// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDMEZowwt8goWDWxmS--v6OSGg5kG5YHbw',
    authDomain: 'hallowed-zoo-199314.firebaseapp.com',
    databaseURL: 'https://hallowed-zoo-199314.firebaseio.com',
    projectId: 'hallowed-zoo-199314',
    storageBucket: 'hallowed-zoo-199314.appspot.com',
    messagingSenderId: '972176825771',
    appId: '1:972176825771:web:e908bb1ac9f42a2f'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
